import React, { Component } from 'react'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import login from '../pages/login'
import AppRoute from './app'
import confirm from '../pages/confirm'
import page2 from '../pages/page2'
import employee from  '../pages/employee'
import flexible from '../pages/Flexible'

export default () => (
    <BrowserRouter>
    <Switch>
    <Route exact path='/' component={login} />
    <Route path='/confirm' component={confirm} />
    <Route path='/page2' component={page2} />
    <Route path='/employee' component={employee} />
    <Route path='/flexible' component={flexible} />
    <AppRoute></AppRoute>
    </Switch>
    </BrowserRouter>
)
