import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './page2.css'
import './ps.css'
import G7 from '../img/Group 7.png'
import { get, post, sever } from '../service/api'
import Modal from "react-responsive-modal"
import FileBase64 from 'react-file-base64'


export default class page2 extends Component {
  constructor(props) {
    super(props)

    this.state = {
      annual: [],
      personal: [],
      sick: [],
      enter: [],
      military: [],
      maternity: [],
      training: [],
      userdata: [],
      files: []
    }
  }
  onOpenModal = async () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
    window.location.reload()
  }
  getFiles(files) {
    this.setState({ files: files, imagePreviewUrl: files[0].base64 })
    console.log(files[0].base64)
  }
  _onClick9 = async () => {

    let userdata = JSON.parse(localStorage.getItem("userdata"))

    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    const { files } = this.state

    const empID = parseInt(userdata[0].empID)
    const obj = {
      empID, image: files[0].base64
    }
    console.log(obj)


    try {
      await post('/insert_profile', obj)
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }
  componentDidMount = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    // this.setState({userdata})
    // console.log('nobar', userdata)
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }

    try {
      const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID })
      this.setState({ annual: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID })
      this.setState({ personal: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID })
      this.setState({ sick: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID })
      this.setState({ enter: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID })
      this.setState({ military: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_6', { empID: userdata[0].empID })
      this.setState({ maternity: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_7', { empID: userdata[0].empID })
      this.setState({ training: res.result })
    } catch (error) {
      alert(error)
    }
  }

  _onClick = async () => {
    const an = parseInt('1')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }

  _onClick2 = async () => {
    const an = parseInt('2')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }

  _onClick3 = async () => {
    const an = parseInt('3')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick4 = async () => {
    const an = parseInt('4')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick5 = async () => {
    const an = parseInt('5')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick6 = async () => {
    const an = parseInt('6')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick7 = async () => {
    const an = parseInt('7')
    const obj = { an }
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an", JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
      alert(error)
    }
  }
  _onClick8 = async () => {

    this.props.history.push('/flexible')

  }
  logout = async () => {
    this.props.history.push('/')
    window.location.reload()
  }

  render() {
    const { open } = this.state;
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className='imgUpload' src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">Upload Receipt</div>);
    }
    return (

      <div >
        <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='headerModal'>
            <p className='textheadermodall'>เปลี่ยนรูปโปรไฟล์</p>
          </div>
          <div className='solidmodal123' />
          <div style={{ textAlign: 'center' }}>
            <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
              <div>{$imagePreview}</div>
              <p className='showww'><FileBase64 width={'100px'} multiple={true} onDone={this.getFiles.bind(this)} /> </p>
            </div>
            <div style={{ marginTop: '20px' }}>
              <input type="button" value="Cancle" className="App-button1" onClick={this.onCloseModal} />
              <input type="button" value="Enter" className="App-button" onClick={this._onClick9} />
            </div>
          </div>
        </Modal>


        <div className='justify-center'>

          <div className='p_headder'>
            <div class="col-md-4"></div>
            <div class="col-md-4" style={{textAlign:'center'}}>Leave Request & Remaining </div>
            <div class="col-md-4" onClick={this.logout} style={{textAlign:'right',fontSize:'20px',paddingTop:'7px',height:'100%', cursor: 'pointer'}}>
             <p className='lo'>Logout</p> </div>
          </div>

          <div className='widthstand'>

            <div className='navbarrightsss1'>
              <div className="dro"><img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} /> </div>
              <div className="dropbtn1" >
                <p style={{ color: '#05cdff' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                <h4 style={{ color: 'black' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].userName : null}</h4>
              </div>
            </div>


            <div className='testcolumn col-md-12'>
              <div style={{ textAlign: '-webkit-center' }}>
                <div className='solid'></div>
              </div>
              <div><h3 className='h33'>Public Holiday</h3></div>

              <div className='col-md-6'>
         
               <div className='col10' onClick={this._onClick}>
                  <div className='colleft'> <p style={{ fontWeight: 'bold'}}> Annual Leave</p> <p> ลาหยุดประจำปี </p> </div>
                <div className='colright'><span> {this.state.annual+ "/10" }</span> Days</div> 
              </div>
            </div>
            <div className='col-md-6'>
       
            <div className='col10' onClick={this._onClick4}>
                <div className='colleft'><p style={{ fontWeight: 'bold'}}>Enter for monkhod</p> <p>ลาบวช</p></div>
                <div className='colright'><span>{this.state.enter + "/15" }</span>  Days</div>
              </div>

            <div className='col-md-6'>
          
            <div className='col10' onClick={this._onClick2}>
                <div className='colleft'><p style={{ fontWeight: 'bold'}}>Personal Leave</p> <p> ลากิจ </p></div>
                <div className='colright'><span>{this.state.personal + "/5"}</span>  Days</div>
              </div>
            </div>
            <div className='col-md-6'>
      
            <div className='col10' onClick={this._onClick5}>
                <div className='colleft'><p style={{ fontWeight: 'bold'}}>Military Leave </p> <p> ลาเพื่อรับราชการ</p></div>
                <div className='colright'><span>{ this.state.military + "/60" }</span>  Days</div>
              </div>

            <div className='col-md-6'>
           
              <div className='col10' onClick={this._onClick3}>
                <div className='colleft'><p style={{ fontWeight: 'bold'}}>Sick Leave</p> <p>ลาป่วย</p></div>
                <div className='colright'><span>{this.state.sick+ "/30"}</span>  Days</div>
              </div>
            </div>
            <div className='col-md-6'>
     
              <div className='col10' onClick={this._onClick6}>
                <div className='colleft'><p style={{ fontWeight: 'bold'}}>Maternity Leave</p> <p> ลอคลอด</p></div>
                <div className='colright'><span> { this.state.maternity+ "/90" }</span>  Days</div>
              </div>
              <div className='col-md-6'>

                <div className='col10' onClick={this._onClick8}>
                  <div className='colleft1'><p style={{ fontWeight: 'bold' }}>Flexible Benefit Request</p> <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ</p></div>
                  {/* <div className='colright'><p> </p></div> */}
                </div>
              </div>
            </div>
            <div className='col-md-6'  >
            
              <div className='col10' onClick={this._onClick7}>
                <div className='colleft'><p style={{ fontWeight: 'bold'}}>Trainig Leave</p> <p>ลาเพื่อฝึกอบรม</p></div>
                <div className='colright'><span> { this.state.training + "/5"}</span>  Days</div>
              </div>

            </div>
          </div>
        </div>

        <footer className="p_footer">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div >
    );
  }
}
