import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import './ps.css'
import G7 from '../img/Group 7.png'
import { get, post,sever } from '../service/api'

export default class leave_request extends Component {

  constructor(props) {
    super(props)

    this.state = {
      annual: [],
      personal: [],
      sick: [],
      enter: [],
      military: [],
      maternity: [],
      training: [],
      userdata: [],
    }
  }

  componentDidMount = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    // this.setState({userdata})
    // console.log('nobar', userdata)
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }

    try {
      const res = await post('/show_my_leave_request_1', { empID: userdata[0].empID })
      this.setState({ annual: res.result })
      console.log(this.setState({ annual: res.result }))
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_2', { empID: userdata[0].empID })
      this.setState({ personal: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_3', { empID: userdata[0].empID })
      this.setState({ sick: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_4', { empID: userdata[0].empID })
      this.setState({ enter: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_5', { empID: userdata[0].empID })
      this.setState({ military: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_6', { empID: userdata[0].empID })
      this.setState({ maternity: res.result })
    } catch (error) {
      alert(error)
    }
    try {
      const res = await post('/show_my_leave_request_7', { empID: userdata[0].empID })
      this.setState({ training: res.result })
    } catch (error) {
      alert(error)
    }
  }

  _onClick = async () => {
    const an =  parseInt('1')
    const obj = {an}
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an",JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
        alert(error)
    }
  }

  _onClick2 = async () => {
    const an =  parseInt('2')
    const obj = {an}
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an",JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
        alert(error)
    }
  }

  _onClick3 = async () => {
    const an =  parseInt('3')
    const obj = {an}
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an",JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
        alert(error)
    }
  }
  _onClick4 = async () => {
    const an =  parseInt('4')
    const obj = {an}
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an",JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
        alert(error)
    }
  }
  _onClick5 = async () => {
    const an =  parseInt('5')
    const obj = {an}
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an",JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
        alert(error)
    }
  }
  _onClick6 = async () => {
    const an =  parseInt('6')
    const obj = {an}
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an",JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
        alert(error)
    }
  }
  _onClick7 = async () => {
    const an =  parseInt('7')
    const obj = {an}
    try {
      const res = await post('/show_my_leave_request_8', obj)
      localStorage.setItem("an",JSON.stringify(res.result))
      this.props.history.push('/employee')
    } catch (error) {
        alert(error)
    }
  }
  _onClick8 = async () => {
   
      this.props.history.push('/flexible')
   
  }

  render() {
    return (
      <div>

        <div className='navbarright1' >
          <div className="droo" >
          <img className='dro2' src={sever + '/user_profiles'} /> </div>
          <div className="dropbtn1" >
            <p style={{ fontWeight: 'bold', color: '#05cdff', margin: 'unset' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
            <h5 style={{ fontWeight: 'bold', color: 'black', margin: 'unset' }}>{this.state.userdata.length > 0 ? this.state.userdata[0].userName : null}</h5>
          </div>

        </div>

        <div /*className='justifycenter'*/>
          <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <div className='solid'></div>
            {/*----------------  */}

            <div style={{ display: 'inline-flex' }}>

              <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                <div className='block'  onClick={this._onClick}>
                  <div className='contentleft'>
                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Annual Leave </p>
                    <p> ลาพักร้อน </p>
                  </div>
                  <div className='contentright'>
                    <span> {this.state.annual + "/10" } </span>
                    <label>Days</label>
                  </div>
                </div>
              </div>

              <div>
                <div className='block'  onClick={this._onClick4}>
                  <div className='contentleft'>
                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Enter for monkhood </p>
                    <p>  ลาบวช </p>
                  </div>
                  <div className='contentright'>
                    <span>{this.state.enter + "/15"} </span>
                    <label>Days</label>
                  </div>
                </div>
              </div>
            </div>
            {/*----------------  */}
            <div style={{ display: 'inline-flex' }}>
              
                <div style={{ marginRight: '20px', marginBottom: '20px' }} >
                  <div className='block' onClick={this._onClick2}>
                    <div className='contentleft'>
                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Personal Leave </p>
                      <p> ลากิจ </p>
                    </div>
                    <div className='contentright'>
                      <span > {this.state.personal + "/5"} </span>
                      <label>Days</label>
                    </div>
                  </div>
                </div>
              <div>
                <div className='block'  onClick={this._onClick5}>
                  <div className='contentleft'>
                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Military Leave </p>
                    <p> ลาเพื่อรับราชการทหาร </p>
                  </div>
                  <div className='contentright'>
                    <span> {this.state.military + "/60" } </span>
                    <label>Days</label>
                  </div>
                </div>
              </div>
            </div>
            {/*----------------  */}
            <div style={{ display: 'inline-flex' }}>
              
                <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                  <div className='block'  onClick={this._onClick3}>
                    <div className='contentleft'>
                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Sick Leave </p>
                      <p> ลาป่วย </p>
                    </div>
                    <div className='contentright'>
                      <span> {this.state.sick  + "/30" } </span>
                      <label>Days</label>
                    </div>
                  </div>
                </div>
              <div>
                <div className='block'  onClick={this._onClick6}>
                  <div className='contentleft'>
                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> meternity leave </p>
                    <p> ลาคลอด </p>
                  </div>
                  <div className='contentright'>
                    <span> {this.state.maternity + "/90" } </span>
                    <label>Days</label>
                  </div>
                </div>
              </div>
            </div>
            {/*----------------  */}
            <div style={{ display: 'inline-flex' }}>
             
                <div style={{ marginRight: '20px', marginBottom: '20px' }}>
                  <div className='block' onClick={this._onClick8}>
                    <div style={{ padding: '0 10px', color: 'black' }}>
                      <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Flexible Benefit Request </p>
                      <p> คำขอเกี่ยวกับสวัสดิการอื่นๆ </p>
                    </div>
                  </div>
                </div>
              <div>
                <div className='block'  onClick={this._onClick7}>
                  <div className='contentleft'>
                    <p style={{ fontWeight: 'bold', marginBottom: '0px' }}> Training Leave </p>
                    <p> ลาเพื่อฝึกอบรม  </p>
                  </div>
                  <div className='contentright'>
                    <span> {this.state.training + "/5" } </span>
                    <label>Days</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
