import React, { Component } from 'react';
import './Flexible.css'
import { post } from '../service/api'
import FileBase64 from 'react-file-base64'
export default class Flexible extends Component {


  constructor(props) {
    super(props)

    this.state = {
      ben_type: '',
      // ben_date_request: '',
      ben_date_receipt: '',
      amount: '',
      files: []

    }

  }

  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
    // console.log(this.state.userName)
  }

  getFiles(files) {
    this.setState({
      files: files,
      imagePreviewUrl: files[0].base64
    });
    console.log(files[0].base64)
  }

  _onClick2 = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
      if (userdata[0].role_id == 1) {
        this.props.history.push('/leaverequest')
      } else {
        this.props.history.push('/page2')
      }
    } else {
      this.setState({ userdata: [] })
    }
  }
 
  render() {

    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className='imgUpload' src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">Upload Receipt</div>);
    }

    return (

      <div style={{ marginBottom: '50px' }}>
        <div className='f_header'>Flexible Benefit Request</div>

        <div className='App-Content'>

          <h4 className='headtext'>Flexible Benefit Request</h4>
          <p>
            <select className='select' name="ben_type" id="Flexible Benefit Request" onChange={this._onChange} >
              <option >Perpose for Flexible Benefit</option>
              <option value="1">Optical Purchasing</option>
              <option value="2">Oral Purchasing</option>
              <option value="3">Sport</option>
              <option value="4">Transportation on vacation</option>
            </select>
          </p>
          {/* <p>
            <input type="date" name="ben_date_request" className="App-date2" onChange={this._onChange} />
          </p> */}
          <p>
            <input type="date" name="ben_date_receipt" className="App-date2" onChange={this._onChange} />
          </p>

          <p className='borderamount'  >
            <label className="textamount" for="fname" onChange={this._onChange}  >Amount</label>
            <input className="inputamount" id="fname" name="amount" placeholder="0.00" onChange={this._onChange} />
          </p>
          <div >
          {$imagePreview} </div>
          < div style={{textAlign: '-webkit-center',paddingLeft:'20px'}}>
            <FileBase64 multiple={true} onDone={this.getFiles.bind(this)} />
          </div>

          <p>
            <button className="button button4" onClick={this._onClick2} >Cancel</button>
            <button className="button button4" onClick={this._onClick}>Submit </button>
          </p>
        </div>
        <footer className="p_footer">ProjectSoft (Thailand).Co.,Ltd. </footer>
      </div>

    );
  }
  _onClick = async () => {

    let userdata = JSON.parse(localStorage.getItem("userdata"))

    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    const { ben_type, ben_date_receipt, amount, files } = this.state

    const empID = parseInt(userdata[0].empID)
    const obj = {
      ben_type, ben_date_receipt, amount, empID, image: files[0].base64
    }
    console.log(obj)


    try {
      await post('/insert_flexible', obj)
      alert('create success')
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }
}

