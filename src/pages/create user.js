import React, { Component } from 'react'
import '../pages/create.css'
import { post } from '../service/api'

export default class user extends Component {

    constructor(props) {
        super(props)

        this.state = {
            userName: '',
            emp_name: '',
            emp_last: '',
            email: '',
            pro_period: '1',
            role: '1',
        }
    }

    _onChange = e => {
        this.setState({ [e.target.name]: e.target.value })

    }

    render() {
        return (
            <div>
                <div className='cu1'>
                    <h3 > Create User Account</h3>
                </div>
                <div className="App-User">

                    <input type="text" name="userName" placeholder="UserName" className="App-ub" onChange={this._onChange} />
                    <input type="text" name="emp_name" placeholder="Name" className="App-ub" onChange={this._onChange} />
                    <input type="text" name="emp_last" placeholder="Lastname" className="App-ub" onChange={this._onChange} />
                    <input type="text" name="email" placeholder="Email" className="App-ub" onChange={this._onChange} />



                    <select name="pro_period" placeholder="Probation Period" className="App-ub1" onChange={this._onChange} /*value={this.state.role_ID}*/ >
                        <option value="1">Pass</option>
                        <option value="2">4 Month</option>
                    </select>


                    <select name="role" placeholder="User Role" className="App-ub2" onChange={this._onChange} /*value={this.state.pro_period_ID}*/>
                        <option value="1">Admin</option>
                        <option value="2">Employee</option>
                    </select>

                    <div flexDirection="row" className='cu2'>
                        <input type="button" value="Cancle" className="App-button1" />
                        <input type="button" value="Create Account" className="App-button" onClick={this._onClick} />
                    </div>

                </div>
            </div>

        )
    }

    _onClick = async () => {
        let userdata = JSON.parse(localStorage.getItem("userdata"))
        console.log('nobar', userdata)
        if (userdata) {
            this.setState({ userdata })
        } else {
            this.setState({ userdata: [] })
        }

        const { userName, emp_name, emp_last, email, role,pro_period  } = this.state
        const user_create_id = parseInt(userdata[0].empID)
        const obj = {
            userName, emp_name, emp_last, email, user_create_id,role, pro_period
        }
    
        console.log(obj)
        try {
            await post('/insert_user', obj)
            alert('create success')
            window.location.reload()
        } catch (error) {
            alert(error)
        }

    }
}
