import React, { Component } from 'react';
import './Leave.css';
import './modal.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'antd/lib/radio';
import { Row } from 'antd';
import searc from '../img/magnifying-glass.png'
import Modal from "react-responsive-modal"
import { get, sever } from '../service/api'
import { post } from '../service/api'
import moement from 'moment';


export default class flexibleAm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false,
      dataTables: [],
      showdata: [],
      showdata_2: [],
      benID:0

    }

  }
  onOpenModal = async (cell) => {
    console.log(cell)
    this.setState({
      benID : cell
    })
    try {
      const res = await post('/show_benefit_1', { benID: cell })
      this.setState({ open: true, showdata: res.result })
    } catch (error) {
      alert(error)
    }
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }
  componentDidMount = async () => {
    try {
      const res = await get('/show_benefit')
      this.setState({ dataTables: res.result })
    } catch (error) {
      alert(error)
    }
  }
  ButtonApprove = (cell, Row) => {
    
    return (
      <div>
        <img src={searc} style={{ marginRight: '10px' }} onClick={() => this.onOpenModal(cell)} />
        <button style={{ border: '1px', borderRadius: '5px', height: '30px' }} onClick={() => this.onUpdate(cell)}>Approve</button>

      </div>
    )
  }
  onUpdate = async (cell, benID) => {
    console.log(cell, benID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    console.log(userdata)

    try {
      await post('/update_benefit', { add_empID: userdata[0].empID, benID: cell })
      alert('update success')
      window.location.reload()
    } catch (error) {
      alert(error)
    }

  }

  onUpdate_1 = async (benID) => {
    console.log(benID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    try {
      await post('/update_benefit_1', {add_empID: userdata[0].empID,benID})
      alert('update success')
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }
  onUpdate_2 = async ( benID) => {
    console.log(benID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    console.log(userdata)

    try {
      await post('/update_benefit', { add_empID: userdata[0].empID, benID })
      alert('update success')
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }
  render() {
    const { open } = this.state;
    return (
      <div>
        <h3>
          <h2>
            <div className='lea'>
              <p>Flexible Benafit</p>
            </div>
          </h2>
        </h3>

        <div style={{marginBottom:'20px'}}>
          <BootstrapTable className='boot' data={this.state.dataTables} hover pagination>
            <TableHeaderColumn width={80} dataField='benID' dataSort isKey>{'ID'}</TableHeaderColumn>
            <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
            <TableHeaderColumn dataField='benName_type' dataSort>{'Benefit Type'}</TableHeaderColumn>
            <TableHeaderColumn dataField='ben_date_request' dataSort dataFormat={(cell) => <p>{moement(cell).format('YYYY-MM-DD')}</p>}>{'Date Request'}</TableHeaderColumn>
            <TableHeaderColumn dataField='ben_date_receipt' dataSort dataFormat={(cell) => <p>{moement(cell).format('YYYY-MM-DD')}</p>}>{'Date Receipt'}</TableHeaderColumn>
            <TableHeaderColumn dataField='amount' dataSort>{'Amount'}</TableHeaderColumn>
            <TableHeaderColumn dataField='sta_name' dataSort>{'Status'}</TableHeaderColumn>
            <TableHeaderColumn dataField='nameuser' dataSort>{'Approver'}</TableHeaderColumn>
            <TableHeaderColumn dataField='flexible_created' dataSort dataFormat={(cell) => <p>{moement(cell).format('YYYY-MM-DD')}</p>}>{'Created'}</TableHeaderColumn>
            <TableHeaderColumn dataField='benID' dataSort dataFormat={this.ButtonApprove} >{'Action'}</TableHeaderColumn>
          </BootstrapTable>
        </div>

        <Modal  open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='headerModal'>
            <p className='textheadermodal'>Flexible Benefit</p>
          </div>
          <div className='F_solidmodal' />

          <p className='PerposeModal'>{this.state.showdata.length > 0 ? this.state.showdata[0].benName_type : null}</p>

          <div className='datemodalclass'>

            <p className='txtReceiptmodal'>Date Request</p>
            <p className='dateRequestModal'>{moement(this.state.showdata.length > 0 ? this.state.showdata[0].ben_date_request : null).format('YYYY-MM-DD')}</p>

          </div>
          <div className='datemodalclass'>

            <p className='txtReceiptmodal'>Date Receipt</p>
            <p className='dateRecieptModal'>{moement(this.state.showdata.length > 0 ? this.state.showdata[0].ben_date_receipt : null).format('YYYY-MM-DD')}</p>

          </div>
          
          <div className='F_img1' ><img className='F_img'  src={sever+'/bill/'+this.state.benID }/></div>

          <div flexDirection="row" className='buttonModal'>

            <div type="button" value="Reject" className="mdbutton" onClick={() => this.onUpdate_1(this.state.showdata[0].benID)}><p>Reject</p></div>

            <div type="button" value="Approve" className="mdbutton2" onClick={() => this.onUpdate_2(this.state.showdata[0].benID)}><p>Approve</p></div>

          </div>
        </Modal>

      </div>
    );
  }
}