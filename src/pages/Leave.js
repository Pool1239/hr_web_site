import React, { Component } from 'react';
import './Leave.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'antd/lib/radio';
import { Row } from 'antd';
import searc from '../img/magnifying-glass.png'
import { get } from '../service/api'
import { post } from '../service/api'
import moement from 'moment';
import Modal from "react-responsive-modal"

export default class Leave extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dataTables: [],
      showdata : [],
    }
  }
  state = {
    open: false,
  };

  onOpenModal = async (cell) => {
    console.log(cell)
    try {
      const res = await post('/show_request', {leaveID:cell})
        this.setState({ open: true , showdata : res.result  }) 
    } catch (error) {
        alert(error)
    }
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  componentDidMount = async () => {
    try {
      const res = await get('/show_leave_request')
      this.setState({ dataTables : res.result })
    } catch (error) {
      alert(error)
    }
  }

  ButtonApprove=(cell,Row)=>{
    return(
      <div>
      <img src={searc} style={{marginRight:10}} onClick={()=>this.onOpenModal(cell)} />
      <button style={{border:'1px',borderRadius:'5px',height:'30px'}} onClick={()=>this.onUpdate(cell)}>Approve</button>
      </div>
    )
  }
  onUpdate = async (cell,leaveID) => {
    console.log(cell,leaveID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    try {
       await post('/update_leave_request', { add_empID: userdata[0].empID,leaveID:cell})
       alert('update success')
       window.location.reload()
    } catch (error) {
        alert(error)
    }
  }

  onUpdate_1 = async (leaveID) => {
    console.log(leaveID)
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    try {
       await post('/update_leave_request_1',{add_empID: userdata[0].empID,leaveID})
       alert('update success')
       window.location.reload()
    } catch (error) {
        alert(error)
    }
  }

  render() {
    const { open } = this.state;
    return (
      <div>
        <h3>
          <div className='lea'>
            <p>Leave Request</p>
          </div>
        </h3>
        <div style={{marginBottom:'20px'}}>
          {/* <BootstrapTable
            data={this.product} hover
            pagination>
            <TableHeaderColumn dataField='interval_value' isKey>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
            <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
          </BootstrapTable> */}
          <BootstrapTable className='boot' data={this.state.dataTables} hover pagination>
          <TableHeaderColumn width={80}  dataField='Empid' dataSort isKey>{'ID'}</TableHeaderColumn>
          <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
          <TableHeaderColumn dataField='leaName_type' dataSort>{'Leave Type'}</TableHeaderColumn>
          <TableHeaderColumn width={80} dataField='leaName_day' dataSort>{'Type'}</TableHeaderColumn>
          <TableHeaderColumn width={120} dataField='date_start' dataSort dataFormat={(cell) => <p>{moement(cell).format('YYYY-MM-DD')}</p>}>{'From'}</TableHeaderColumn>
          <TableHeaderColumn width={120} dataField='date_end' dataSort dataFormat={(cell) => <p>{moement(cell).format('YYYY-MM-DD')}</p>}>{'To'}</TableHeaderColumn>
          <TableHeaderColumn width={180} dataField='full_day' dataSort >{'No. of days'}</TableHeaderColumn>
          <TableHeaderColumn dataField='sta_name' dataSort>{'Status'}</TableHeaderColumn>
          <TableHeaderColumn dataField='nameuser' dataSort>{'Approver'}</TableHeaderColumn>
          <TableHeaderColumn width={110} dataField='date_created' dataSort dataSort dataFormat={(cell) => <p>{moement(cell).format('YYYY-MM-DD')}</p>}>{'Created'}</TableHeaderColumn>
          <TableHeaderColumn dataField='leaveID' dataSort dataFormat={this.ButtonApprove} >{'Action'}</TableHeaderColumn>
        </BootstrapTable>
        </div>

        <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='headerModal'>
          <p className='textheadermodal'>{this.state.showdata.length > 0 ? this.state.showdata[0].rol_name: null}</p>
            <div style={{fontSize:'xx-large'}}>{this.state.showdata.length > 0 ? this.state.showdata[0].full_name: null}</div>
          </div>

          <div className='headerModal2'>
            <p className='textheadermodal2'>Leave Request</p>
          </div>

          <div className='solidmodal'/>

          <p className='nameModal'>{this.state.showdata.length > 0 ? this.state.showdata[0].leaName_type: null}</p>
          <p className='nameModal'>{this.state.showdata.length > 0 ? this.state.showdata[0].leaName_day: null}</p>
          
          <div className='datemodalclass'>

                  <p  className='dateModal1'>{moement(this.state.showdata.length > 0 ? this.state.showdata[0].date_start: null).format('YYYY-MM-DD')}</p>
                  <p className='dateModal2'>{moement(this.state.showdata.length > 0 ? this.state.showdata[0].date_end: null).format('YYYY-MM-DD')}</p>

          </div>
            
          <p className='ModalReason'>{this.state.showdata.length > 0 ? this.state.showdata[0].lea_reason: null}</p>

          <div flexDirection="row"  className='buttonModal1'>

                <button style={{border:'1px',borderRadius:'5px',height:'30px',width:'70px'}} onClick={()=>this.onUpdate_1(this.state.showdata[0].leaveID)}>Reject</button>
            
                <button style={{border:'1px',borderRadius:'5px',height:'30px',width:'70px'}} onClick={()=>this.onUpdate(this.state.showdata[0].leaveID)}>Approve</button>
        
          </div>
        </Modal>
      </div>
    );
  }
}