import React, { Component } from 'react'
import '../pages/employee.css'
import { Checkbox } from 'antd';
import { post } from '../service/api'

export default class employee extends Component {

  constructor(props) {
    super(props)

    this.state = {
      lea_type: '',
      lea_day: '1',
      date_start: '',
      date_end: '',
      lea_reason: '',
      userdatas:[]
    }
  }

  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
    // console.log(this.state.userName)
  }
  componentDidMount = async () => {

    let userdatas = JSON.parse(localStorage.getItem("an"))
    if (userdatas) {
      this.setState({ userdatas })
    } else {
      this.setState({ userdatas: [] })
    }

  }

  render() {
    return (
      <div>
         
         
        <p className='em3'>
        <div className='p_headder'>Leave Request</div>
          <div className='em1'>
            Leave Request
        <div className="em"></div>
          </div>
        </p>
        <div className='emall'>
          <div className='block1'>
            <p> {this.state.userdatas.length > 0 ? this.state.userdatas[0].leaName_type : null} </p>
          </div>

          <div className='emall'>
            <select id="country" name="lea_day" placeholder="Type" className='em4' onChange={e => this.setState({ lea_day: e.target.value })}>
              <option value="1">Full Day</option>
              <option value="2">Haft Day</option>
            </select>
          </div>
          {
            this.state.lea_day == 1 ?
              <div>
                <div className='emall'>
                  <input type="date" name="date_start" id="dateofbirth" placeholder="From" className='em6' onChange={this._onChange} />
                  <input type="date" name="date_end" id="dateofbirth" placeholder="From" className='em6' onChange={this._onChange} />
                </div>

              </div> :
              <div>
                <div style={{ marginBottom: '10px' }}>
                  <div className='ch' ><input type="datetime-local" className='em7' name="date_start" onChange={this._onChange} /></div>
                </div>

                <div>
                  <div className='time2' ><input type="datetime-local" className='em7' name="date_end" onChange={this._onChange} /> </div>
                </div>

              </div>
          }
          <div className='emall'>
            <textarea name="lea_reason" placeholder="Leave Reason" className='em5' onChange={this._onChange} />
          </div>
          <div className='emall'>
            <input type="button" value="Cancle" className="embt"  onClick={this._onClick2}/>
            <input type="button" value="Submit" className="embt" onClick={this._onClick} />
          </div>
         
        </div>
        <footer className="p_footer">ProjectSoft (Thailand).Co.,Ltd. </footer> 
      </div>



    )
  }
  _onClick = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    let userdatas = JSON.parse(localStorage.getItem("an"))
    if (userdatas) {
      this.setState({ userdatas })
    } else {
      this.setState({ userdatas: [] })
    }
    const { lea_day, date_start, date_end, lea_reason } = this.state
    const empID = parseInt(userdata[0].empID)
    const lea_type = parseInt(userdatas[0].leaID_type)
    const obj = {
      lea_type, lea_day, date_start, date_end, lea_reason, empID
    }
    console.log(obj)


    try {
      await post('/insert_leave', obj)
      alert('create success')
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }
  _onClick2 = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    if (userdata) {
      this.setState({ userdata })
      if(userdata[0].role_id == 1){
        this.props.history.push('/leaverequest')
      }else{
        this.props.history.push('/page2')
      }
    } else {
      this.setState({ userdata: [] })
    }
  }
}
