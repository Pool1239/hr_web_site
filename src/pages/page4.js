import React, { Component } from 'react';
import './Leave.css';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Button } from 'react-bootstrap'
import { Row } from 'antd';
import { Link } from 'react-router-dom';
import searc from '../img/magnifying-glass.png'
import { get } from '../service/api'
import { post } from '../service/api'
import moement from 'moment';

export default class page4 extends Component {
  constructor(props) {
    super(props)
 
    this.state = {
      dataTable: [],
      dataTable2: [],
     
    }
  }

 

  componentDidMount = async () => {
    try {
      const res = await get('/show_user')
      this.setState({ dataTable: res.result })
    } catch (error) {
      alert(error)
    }

    try {
      const res = await get('/show_user2')
      this.setState({ dataTable2: res.result })
    } catch (error) {
      alert(error)
    }
  }


  ButtonApprove = (cell, Row) => {
    return (
      <div>
        <input type="button" value="Delete" className="del" onClick={() => this.delete(cell)} />
      </div>
    )
  }

  delete = async (cell) => {
    console.log(cell)
    try {
      await post('/delete_user', { empID: cell })
      alert('delete success')
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }

  render() {
    return (
      <div>
        <div className='lea'>
          <div><h3>User ({this.state.dataTable2.length > 0 ? this.state.dataTable2[0].sumid : null} users)</h3></div>
          <div><Link to='/create'>+ Create User Accout</Link></div>
        </div>

        <div style={{marginBottom:'20px'}} >
          {/* <BootstrapTable
            data={this.product} hover
            pagination>
            <TableHeaderColumn dataField='interval_value' isKey>Product ID</TableHeaderColumn>
            <TableHeaderColumn dataField='name'>Product Name</TableHeaderColumn>
            <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
          </BootstrapTable> */}
          <BootstrapTable search className='boot' data={this.state.dataTable} hover pagination>
            <TableHeaderColumn width='80' dataField='empID' dataSort isKey>{'ID'}</TableHeaderColumn>
            <TableHeaderColumn dataField='full_name' dataSort>{'Employee Name'}</TableHeaderColumn>
            <TableHeaderColumn dataField='email' dataSort>{'Email'}</TableHeaderColumn>
            <TableHeaderColumn width='250' dataField='starting_date' dataSort dataFormat={(cell) => <p>{moement(cell).format('YYYY-MM-DD  HH:MM:SS')}</p>}>{'Starting Date'}</TableHeaderColumn>
            <TableHeaderColumn dataField='pro_name' dataSort>{'Probation period'}</TableHeaderColumn>
            <TableHeaderColumn dataField='admin_email' dataSort>{'Approval Email'}</TableHeaderColumn>
            <TableHeaderColumn width='150' dataField='rol_name' dataSort>{'Role'}</TableHeaderColumn>
            <TableHeaderColumn width='100' dataField='empID' dataSort dataFormat={this.ButtonApprove}>{''}</TableHeaderColumn>
          </BootstrapTable>
        </div>
      </div>
    );
  }
}
