import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { Nav, NavItem, NavDropdown, Navbar, MenuItem, Button } from 'react-bootstrap'
import Digio from '../img/digio_logo.png';
import G7 from '../img/Group 7.png'
import Project from '../img/Project.png'
import PS from '../img/123456.png'
import FileBase64 from 'react-file-base64'
import { get, post, sever } from '../service/api'
import user from '../pages/create user';
import Modal from "react-responsive-modal"
import './nobar.css';

class nobar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userdata: [],
      showdata: [],
      open: false,
      files: []
    }
  }

  onOpenModal = async () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
    window.location.reload()
  }

  componentDidMount = async () => {
    let userdata = JSON.parse(localStorage.getItem("userdata"))
    // this.setState({userdata})
    // console.log('nobar', userdata)
    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    // try {
    //   const res = await post('/show_username_my_leave_request',{empID:userdata})
    //   this.setState({ showdata : res.result })
    // } catch (error) {
    //   alert(error)
    // }
    try {

    } catch (error) {
      alert(error)
    }
  }

  getFiles(files) {
    this.setState({ files: files, imagePreviewUrl: files[0].base64 })
    console.log(files[0].base64)
  }

  // componentDidMount = () => {
  //   let userdata = localStorage.getItem("userdata")
  //   if(userdata) {
  //     this.setState({userdata})
  //   } else {
  //     this.setState({userdata: []})
  //   }
  //   console.log('nobar', userdata)

  // }
  _onClick = async () => {

    let userdata = JSON.parse(localStorage.getItem("userdata"))

    if (userdata) {
      this.setState({ userdata })
    } else {
      this.setState({ userdata: [] })
    }
    const { files } = this.state

    const empID = parseInt(userdata[0].empID)
    const obj = {
      empID, image: files[0].base64
    }
    console.log(obj)


    try {
      await post('/insert_profile', obj)
      window.location.reload()
    } catch (error) {
      alert(error)
    }
  }

  logout = async () => {
    this.props.history.push('/')
    window.location.reload()
  }


  render() {
    const { open } = this.state;
    const { pathname } = this.props.location;
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img className='imgUpload' src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">Upload Receipt</div>);
    }
    return (
      <div>
        <div className='ul'>
          {/* =========================== */}
          <div className='navbarleft' >
            <div className='topleft'>
              <img src={PS} />
            </div>
            <div className='topright'>
              <Link to='/leave' style={pathname === '/leave' ? { background: 'rgba(0, 94, 126, 0.76)', height: '100%' } : null} className="dropbtn">Leave Request</Link>
              <Link to='/flexibleAm' style={pathname === '/flexibleAm' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">Flexible Benefit</Link>
              <Link to='/page4' style={pathname === '/page4' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">User</Link>
              <li className="dropdown">
                <Link to='/leavereport' style={pathname === 'leavereport' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">Report</Link>
                <div className="dropdown-content">
                  <Link to='/leavereport' style={pathname === '/leavereport' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">Leave Report</Link>
                  <Link to="/benefitreport" style={pathname === '/benefitreport' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">Flexible Benefit Report</Link>
                </div>
              </li>
              <Link to='/leaverequest' style={pathname === '/leaverequest' ? { background: 'rgba(0, 94, 126, 0.76)' } : null} className="dropbtn">My Leave Request</Link>
            </div>
          </div>
          {/* ==================== */}
          <div className='navbarright'>
            <div className="dro">
              <img className='dro2' src={sever + '/user_profiles'} onClick={this.onOpenModal} />
            </div>
            <li className="dropdown">
              <div className="dropbtn2" >
                <p style={{ margin: '0 0 1px' }}> {this.state.userdata.length > 0 ? this.state.userdata[0].role : null}</p>
                <p> {this.state.userdata.length > 0 ? this.state.userdata[0].userName : null} </p>
                {/* <p>eieieie</p> */}
              </div>
              <div className="dropdown-content">
                <a style={{ cursor: 'pointer' }} onClick={this.logout} > Logout </a>
              </div>
            </li>
          </div>
        </div>
        <Modal open={open} onClose={this.onCloseModal} center className='ModalApprove' >
          <div className='headerModal'>
            <p className='textheadermodall'>เปลี่ยนรูปโปรไฟล์</p>
          </div>
          <div className='solidmodal123' />
          <div style={{ textAlign: 'center' }}>
            <div style={{ display: 'flex', justifyContent: 'center',flexDirection:'column' }}>
              <div>{$imagePreview}</div>
              <p className='showww'><FileBase64 width={'100px'} multiple={true} onDone={this.getFiles.bind(this)} /> </p>
            </div>
            <div style={{ marginTop: '20px' }}>
              <input type="button" value="Cancle" className="App-button1" onClick={this.onCloseModal} />
              <input type="button" value="Enter" className="App-button" onClick={this._onClick} />
            </div>
          </div>
        </Modal>
      </div>

    );
  }
}
export default withRouter(nobar)